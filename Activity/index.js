/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

function printName (){
	let firstName = prompt('Enter your first name: ');
	let lastName = prompt('Enter your last name: ');
	let Age = prompt('Enter your age: ');
   let location = prompt('Enter your location: ');

	console.log('Name: ' + firstName);
	console.log('Last Name: ' + lastName);
	console.log('Age: ' + Age);
	console.log('location: ' + location);

	

};

printName();

	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printMusicalartists (){

	   let firstArtist = console.log('your first favorite musical artist: Alan Walker');
	  

	   let secondArtist = console.log('your fifth favorite musical artist: KSHMR');
	

	   let thirdArtist = console.log('your fifth favorite musical artist: Avicii');
	  

	   let fourthArtist = console.log('your fifth favorite musical artist: Illenium');
	  

	   let fifthArtist = console.log('your fifth favorite musical artist: K-391');

	

};

printMusicalartists();

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.


	
*/
function favoriteMovies ()
      
     
{
     

	   let firstMovie = console.log('your first favorite Movie: Me Before you');
	   console.log("Rotten Tomatoes Rating: 54% ");

	   let secondMovie = console.log('your fifth favorite Movie: Let me in');
	   console.log("Rotten Tomatoes Rating: 88% ");


	   let thirdMovie = console.log('your fifth favorite Movie: Book of Eli');
	   console.log("Rotten Tomatoes Rating: 47%");


	   let fourthMovie = console.log('your fifth favorite Movie: Pitch Black');
	   console.log("Rotten Tomatoes Rating: 59%");


	   let fifthMovie = console.log('your fifth favorite Movie: Chronicles of Riddick');
	   console.log("Rotten Tomatoes Rating: 29% \n");
};


favoriteMovies();

	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers()
{
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	
console.log(friend1);
console.log(friend2);
console.log(friend3);
};


 
printFriends();
