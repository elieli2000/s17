function printName()
{
	console.log('Damn son');
};

printName();

function declaredFunction()
{
	console.log('This is a define function');
};

declaredFunction();


function declaredFunction2()
{
	console.log("Hi I am from declared function().");

};

declaredFunction2();
declaredFunction2();
declaredFunction2();

let variableFunction = function()
{
	console.log('I am from variable function.');
};

variableFunction();

let funcExpression = function funcName(){
	console.log('hello');
};

funcExpression();

declaredFunction = function(){
	console.log('updated declared function');

};
declaredFunction();

funcExpression = function(){
	console.log('updated function Expression')
};

funcExpression();

const constantFunction = function()
{
	console.log('Initialized with const.');
};

constantFunction();

{
	let localVar = "Local variable";
}
let globalVar = "This is Global Variable";

console.log(globalVar);


function showNames() //local
{
	var functionVar = 'John';
	const functionConst = 'Band';
	let functionLet = 'guitar';

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
};

showNames();

function myNewFunction(){
	let name = 'Yor';

	function nestedFunction(){
		let nestedName = 'Brando';
		console.log(nestedName);

	};
	nestedFunction();
};
myNewFunction();

/*function printWelcomeMessage (){
	let firstName = prompt('Enter your first name: ');
	let lastName = prompt('Enter your last name: ');
	console.log('Hello, ' + firstName + ' ' + lastName + '!');
	console.log("Welcome Traveller!");

};

printWelcomeMessage();

function getCourses(){
	let courses = ['HTML', 'Science', 'Grammar', 'Mathematics'];

	console.log(courses);

getCourses();

function get(){
	let name = "Eli"
	console.log(name);
};

get();
};*/

function displayCarInfo()
{
console.log('Brand: Toyota');
console.log('Type: Sedan');
console.log('Price: 1,500,000');
};

displayCarInfo();